package string.string4;

public class RegexPatterns {

    private static final String COMMENT_REGEX_MULTI_AND_SINGLE_LINE = "(/\\*([^*]|[\\r\\n]|(\\*+([^*/]|[\\r\\n])))*\\*+/|[\\t]*//.*)|\"(\\\\.|[^\\\\\"])*\"|'(\\\\[\\s\\S]|[^'])*'";


    public static String getCommentRegexMultiAndSingleLine() {
        return COMMENT_REGEX_MULTI_AND_SINGLE_LINE;
    }
}
