package string.string4;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class String4 {
    /**
     * @param args
     */
    /*
   коментарий
     */
    //
    public static void main(String[] args) {
        String code = "код код /**коментарий*/ \n" +
                "//код в коментарии \n" +
                "код код";
        code = clean(code);
        System.out.println(code);
    }
    public static String clean(String code){
        Pattern pat=Pattern.compile("(?s)(?>\\/\\*(?>(?:(?>[^*]+)|\\*(?!\\/))*)\\*\\/)(?://.*)|(/\\\\*(?:.|[\\\\n\\\\r])*?\\\\*/)");
        Matcher match=pat.matcher(code);
        code = match.replaceAll("");
        return code;
    }
}
