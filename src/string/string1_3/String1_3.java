package string.string1_3;

import java.util.HashMap;
import java.util.Map;

public class String1_3 {
    public static void main(String[] args) {
        StringBuilder buffer = new StringBuilder("Привет мир");
        String line2 = "Hello world";
        String line3 =  "Привет мир, привет страна";
        String pattern = "(?i)[aeiou]";

        System.out.print("1) " + buffer);
        System.out.println(" - " + buffer.reverse());

        System.out.println("2) " + line2 + " - " + line2.replaceAll(pattern, "").length());

        search(line3);
    }
    public static void search(String line){
        String [] words = line.toLowerCase().replaceAll("[-.?!)(,:]", "").split("\\s");
        Map<String, Integer> counterMap = new HashMap<>();
        for (String word : words) {
            if(!word.isEmpty()) {
                Integer count = counterMap.get(word);
                if(count == null) {
                    count = 0;
                }
                counterMap.put(word, ++count);
            }
        }

        System.out.println("3)");
        for(String word : counterMap.keySet()) {
            System.out.println(word + " - " + counterMap.get(word));
        }
    }
}
