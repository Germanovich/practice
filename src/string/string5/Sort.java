package string.string5;

import java.util.Scanner;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Sort {
    public static void sortLexemes(String text) {


        Scanner in = new Scanner(System.in);
        System.out.print("Input a symbol: ");
        char symbol = in.next(".").charAt(0);
        String stringSimbol = String.valueOf(symbol);

        String[] words = text.split("\\s+");
        for (int i = 0; i < words.length; i++) {
            words[i] = words[i].replaceAll("[,.]", "");
            words[i] = words[i].toLowerCase();
        }

        int[] array = new int[words.length];

        Arrays.sort(words);

        for (int i = 0; i < words.length; i++) {
            int number = 0;
            String[] buff = words[i].split("");
            for (String s : buff) {
                System.out.print(s);
                if (stringSimbol.equals(s)) {
                    number++;
                }
            }
            array[i] = number;
            System.out.println(" - " + array[i]);
        }

        boolean isSorted = false;

        while (!isSorted) {
            isSorted = true;
            for (int i = 0; i < array.length - 1; i++) {
                if (array[i] < array[i + 1]) {
                    isSorted = false;
                    int bufferInt = array[i];
                    String bufferString = words[i];

                    array[i] = array[i + 1];
                    words[i] = words[i + 1];

                    array[i + 1] = bufferInt;
                    words[i + 1] = bufferString;
                }
            }
        }
        for (String str : words) {
            System.out.print(str + " ");
        }
    }



    static void sortParagraphs(String text) {
        String[] s = text.trim().split("\n");
        int[] arr = new int[s.length];
        int[] k = new int[s.length];

        for (int i = 0; i <= s.length - 1; i++) {
            int t = 0;
            String[] w = s[i].trim().split(" ");
            for (int j = 0; j <= w.length - 1; j++) {
                t++;
            }
            arr[i] = t;
            k[i] = t;
        }

        int min;
        int bugger;
        int index;
        for (int i = 0; i <= arr.length - 1; i++) {
            min = arr[i];
            index = i;
            for (int j = i + 1; j <= arr.length - 1; j++) {
                if (min > arr[j]) {
                    min = arr[j];
                    index = j;
                }
            }
            if (index != i) {
                bugger = arr[i];
                arr[i] = arr[index];
                arr[index] = bugger;
            }
        }
        for (int i = 0; i <= arr.length - 1; i++) {
            for (int j = 0; j <= arr.length - 1; j++) {
                if (arr[i] == k[j]) {
                    System.out.println(s[j]);
                }
            }
        }
    }

    public static void sortText(String text) {

        Matcher matcher = Pattern.compile("([^.!?]+[.!?])").matcher(text);
        while (matcher.find()) {
            String[] words = matcher.group(1).split(" ");
            for (int i = 0; i < words.length; i++) {
                words[i] = words[i].replaceAll("[,.]", "");
            }

            String tmp;

            for (int i = words.length - 1; i > 0; i--) {
                for (int j = 0; j < i; j++) {
                    if (words[j].trim().length() > words[j + 1].trim().length()) {
                        tmp = words[j];
                        words[j] = words[j + 1];
                        words[j + 1] = tmp;
                    }
                }
            }
            String str = String.join(" ", words);
            System.out.println(str);
        }
    }
}
