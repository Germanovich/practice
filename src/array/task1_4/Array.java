package array.task1_4;

import java.util.Scanner;

public class Array {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int[] array1 = new int[5];
        int[] array2 = new int[5];

        for (int i = 0; i < array1.length; i++) {
            array1[i] = (int) (1 + Math.random() * 10);
        }

        for (int i = 0; i < array2.length; i++) {
            System.out.print("array[" + (i + 1) + "] = ");
            array2[i] = scanner.nextInt();
        }

        conclusion(array1);
        conclusion(array2);
    }

    public static void conclusion(int[] array) {
        for (int value : array) {
            System.out.print(value + " ");
        }
        search(array);
    }

    public static void search(int[] array) {

        int min = array[0];
        int max = array[0];
        int average = 0;

        for (int i = 0; i < array.length; i++) {
            average += array[i];

            if (min > array[i]) {
                min = array[i];
            }

            if (max < array[i]) {
                max = array[i];
            }
        }
        average /= array.length;
        System.out.print("Max: " + max + " Min: " + min + " Average: " + average + "\n");
        searchNumber(array);
    }

    public static void searchNumber(int[] array) {
        boolean check = true;

        for (int i = 0; i < array.length; i++) {
            if (array[i] < 10 || array[i] > 99) {
                check = false;
                break;
            }
        }

        if (check) {
            for (int value : array) {
                System.out.print(value + " ");
            }
            System.out.println();
        }
    }
}
