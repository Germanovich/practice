package array.task5;

import java.util.Scanner;

public class Array5 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int[] array = new int[5];
        int[] sizeNumbers = new int[5];
        System.out.println();
        System.out.print("Введите 5 числ через пробел: ");
        for (int i = 0; i < array.length; i++) {
            array[i] = scanner.nextInt();
            sizeNumbers[i] = (array[i] + "").length();
        }
        increasingNumbers(array, sizeNumbers);
    }

    private static void increasingNumbers(int[] array, int[] sizeNumbers) {

        for (int i = 0; i < array.length; i++) {
            int number;
            int k = 0;
            boolean check = false;
            int[] array1 = new int[sizeNumbers[i]];

            number = array[i];
            while (number != 0) {
                array1[k] = number % 10;
                number = number / 10;
                if (array1[k] > number % 10) {
                    check = true;
                } else {
                    check = false;
                    break;
                }
                k++;
            }

            if (check) {
                System.out.println("Число, цифры в котором идут в строгом порядке возрастания: " + array[i]);
            }

        }
    }
}