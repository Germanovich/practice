package array.task8_10;

import java.util.Arrays;
import java.util.Scanner;

public class Array8_10 {
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        System.out.print("Input n (NxN): ");
        int line = in.nextInt();
        int[][] matrix0 = new int[line][];
        int[][] matrix1 = new int[line][line];

        int size = line;
        int number2 = 0;
        int search = size / 2;

        for (int i = 0; i < line; i++) {
            int number = i + 1;
            matrix0[i] = new int[number];
            Arrays.fill(matrix0[i], 1);
        }

        for (int i = 0; i < line; i++) {

            if (i <= search) {
                for (int j = number2; j < size; j++) {
                    matrix1[i][j] = 1;
                }
                if (i < search) {
                    number2++;
                    size--;
                }
            }

            if (i > search) {
                number2--;
                size++;

                for (int j = number2; j < size; j++) {
                    matrix1[i][j] = 1;
                }
            }
        }

        System.out.println("8)");
        for (int i = 0; i < line; i++) {
            for (int j = 0; j < matrix0[i].length; j++) {
                System.out.print(matrix0[i][j] + "\t");
            }
            System.out.println();
        }
        System.out.println("9)");
        for (int i = 0; i < line; i++) {
            for (int j = 0; j < line; j++) {
                System.out.print(matrix1[i][j] + "\t");
            }
            System.out.println();
        }

        System.out.println("10)");
        rotate90(matrix1);

        for (int i = 0; i < line; i++) {
            for (int j = 0; j < line; j++) {
                System.out.print(matrix1[i][j] + "\t");
            }
            System.out.println();
        }

    }

    public static void rotate90(int[][] matrix) {

        for (int i = 0; i < matrix.length; i++) {
            for (int j = i; j < matrix[0].length; j++) {

                int temp = matrix[i][j];
                matrix[i][j] = matrix[j][i];
                matrix[j][i] = temp;
            }
        }
    }
}
