package array.task6_7;

public class Array6 {
    public static void main(String[] arg) {
        int[] array = {6, 2, 4, 3, 5};
        conclusion(array);
        coup(array);
        conclusion(array);
        sort(array);
        conclusion(array);
    }

    public static void coup(int[] array) {
        for (int i = 0; i < array.length / 2; i++) {
            int buffer = array[i];
            array[i] = array[array.length - i - 1];
            array[array.length - i - 1] = buffer;
        }
    }

    public static void sort(int[] array) {
        boolean isSorted = false;
        int buf;
        while (!isSorted) {
            isSorted = true;
            for (int i = 0; i < array.length - 1; i++) {
                if (array[i] > array[i + 1]) {
                    isSorted = false;

                    buf = array[i];
                    array[i] = array[i + 1];
                    array[i + 1] = buf;
                }
            }
        }
    }

    public static void conclusion(int[] array) {
        for (int i : array) {
            System.out.print(i + " ");
        }
        System.out.println();
    }
}
