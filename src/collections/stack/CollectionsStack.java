package collections.stack;

import java.util.Arrays;

public class CollectionsStack implements InStack<Integer> {

    public static void main(String[] args) {
        Integer[] array = {1, 2, 3, 4, 5, 6, 7};
        CollectionsStack stack = new CollectionsStack(array, "str");
        stack.toString();
        stack.empty();
    }

    private static final int DEFAULT_SIZE = 10;
    private static final String LS = System.lineSeparator();
    private final String nameStack;
    private StringBuilder sb = new StringBuilder();

    private Integer[] stackArray;
    private int size;

    CollectionsStack(String nameStack) {
        this(null, nameStack);
    }

    CollectionsStack(Integer[] values, String nameStack) {
        this.nameStack = nameStack;
        this.stackArray = new Integer[DEFAULT_SIZE];
        addAll(values);
        System.out.printf("Сформирован стек %s:%s%s%s", nameStack, LS, toString(), LS);

    }

    private void increaseLength() {
        if (this.size == this.stackArray.length) {
            this.stackArray = Arrays.copyOf(this.stackArray, this.stackArray.length * 3 >> 1 + 1);
        }
    }

    private void addAll(Integer[] values) {
        if (values != null && values.length > 0) {
            for (Integer value : values) {
                push(value);
            }
        }
    }

    @Override
    public boolean empty() {
        System.out.printf("Проверка стека %s на пустоту:%s%s%s", nameStack, LS, this.size == 0 ? "Стэк пустой" : "Стэк не пустой", LS);
        return this.size == 0;
    }

    @Override
    public Integer peek() {
        System.out.printf("Возвращаем верхний элемент из стека %s:%s%s%s", nameStack, LS, this.size == 0 ? "Стэк пустой" : this.stackArray[0], LS);
        return this.size != 0 ? this.stackArray[0] : null;
    }

    @Override
    public Integer pop() {
        Integer value = null;
        if (this.size != 0) {
            value = this.stackArray[0];
            this.stackArray[0] = null;
            System.arraycopy(this.stackArray, 1, this.stackArray, 0, this.stackArray.length - 1);
            this.size--;
        }
        System.out.printf("Стек %s после удаления элемента:%s%s%s", nameStack, LS, toString(), LS);
        return value;
    }

    @Override
    public Integer push(Integer value) {
        if (value != null) {
            increaseLength();
            System.arraycopy(this.stackArray, 0, this.stackArray, 1, this.stackArray.length - 1);
            this.stackArray[0] = value;
            this.size++;
        }
        System.out.printf("Стек %s после добавления элемента:%s%s%s", nameStack, LS, toString(), LS);
        return value;
    }

    @Override
    public String toString() {
        sb.delete(0, sb.length());
        if (this.size > 0) {
            for (int i = 0; i < this.size; i++) {
                sb.append(String.format("%s ", this.stackArray[i]));
            }
        } else {
            sb.append("В стеке нет элементов");
        }
        return sb.toString();
    }

    public Integer sumValues() {
        Integer result = 0;
        if (this.size != 0) {
            for (int i = 0; i < this.size; i++) {
                result += this.stackArray[i];
            }
        }
        System.out.printf("Сумма элементов стека %s равна:%s%s%s", nameStack, LS, result, LS);
        return result;
    }
}

interface InStack<E> {
    boolean empty();

    E peek();

    E pop();

    E push(E value);
}

