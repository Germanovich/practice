package collections.collections;

import java.util.*;

public class CollectionsTask {
    public static void main(String[] args)
    {
        String[] domains = {"Practice", "Geeks",
                "Code", "Quiz"};

        List<String> list = new ArrayList<>(Arrays.asList(domains));

        Collections.sort(list);
        System.out.println("Sort: " + list);

        Collections.reverse(list);
        System.out.println("Sort: " + list);
    }
}

