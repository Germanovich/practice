package collections.collections_2;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class CollectionsTask {
    public static void main(String[] args) {

        Set<String> strSet = new HashSet<>();
        String stringLine = "lo, lo, lo, lo, Lo, LO, win, wIn, Win, wiN";

        strSet.addAll(Arrays.asList(stringLine.toLowerCase().split(", ")));
        System.out.println(strSet);
    }
}
