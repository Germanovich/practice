package collections.collections_3;

import java.util.*;

public class CollectionsTask {

    public static void main(String[] args) {
        ArrayList<String> strSet = new ArrayList<>();

        String stringLine = "lo, lo, lo, lo, Lo, LO, win, win, Win, wiN";


        strSet.addAll(Arrays.asList(stringLine.split(", ")));
        System.out.println(strSet);
        Map<String, Integer> stringIntegerMap = toMap(strSet);


        stringIntegerMap.forEach((key, value) -> {
            System.out.println(key + " (" + value + ")" + "\n");
        });
    }

    /**
     * возвращяет все различные слова с учетом частоты встречаемости.
     */
    public static Map<String, Integer> toMap(ArrayList<String> list) {
        Map<String, Integer> map = new HashMap<>();

        list.forEach(line -> {
            String[] words = line.split(" ");

            for (String word : words) {
                if (map.containsKey(word)) {
                    map.put(word, map.get(word) + 1);
                    continue;
                }
                map.put(word, 1);
            }

        });

        return map;
    }
}
